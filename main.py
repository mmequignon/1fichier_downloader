#!/usr/bin/env python3

import os
import asyncio
from provider_client import ProviderClient
from yaml_parser import YamlParser

API_KEY = "API_KEY"
OUT_FOLDER = "OUT_FOLDER"
URL_FILENAME_KEY = "URL_FILENAME"
SEMAPHORE_DOWNLOAD = 5

whatever = """
series:
    house_of_the_dragon:
        https://1fichier.com/?8gpbxu6f106j5p2t334l&af=600507/House.of.the.Dragon.S01E08.1080p.WEB.H264-CAKES.rar
movies:
    action:
        https://1fichier.com/?8gpbxu6f106j5p2t334l&af=600507/House.of.the.Dragon.S01E08.1080p.WEB.H264-CAKES.rar
"""

def get_api_key():
    api_key = os.environ.get(API_KEY)
    if not api_key:
        print("Api key not found in env")
        exit(1)
    return api_key

def get_out_folder():
    out_folder = os.environ.get(OUT_FOLDER)
    if not out_folder:
        print("Output folder not found in env")
        exit(0)
    return out_folder

def get_filename():
    filename = os.environ.get(URL_FILENAME_KEY)
    if not filename:
        print("Filename not found in env")
        exit(1)
    return filename

def parse_urls(content, dest_folder, folder=None):
    if folder:
        folder_name = os.path.join(dest_folder, folder)
    else:
        folder_name = dest_folder
    if isinstance(content, list):
        yield folder_name, content
    for key, val in content.items():
        parse_urls(key, val, folder_name)

def get_urls(filename):
    with open(filename, "r") as f:
        lines = f.readlines()
    return [line.replace("\n", "") for line in lines]

async def main():
    api_key = get_api_key()
    filename = get_filename() 
    http_client = ProviderClient(api_key)
    out_folder = get_out_folder()
    yaml_parser = YamlParser(filename)
    for folder, urls in yaml_parser.parsed_content.items():
        path = os.path.join(out_folder, folder)
        tasks = [http_client.download(url, path) for url in urls]
        await asyncio.gather(*tasks)
    


if __name__ == "__main__":
    main()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
