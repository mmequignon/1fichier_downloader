import aiohttp 

class HttpClient:

    def __init__(self, base_url):
        self.base_url = base_url

    async def post(self, url, json=None):
        headers = self._get_headers()
        async with aiohttp.ClientSession(headers=headers) as client:
            async with client.post(url, json=json) as query:
                return await query.json() 

    async def get(self, url, json=None):
        headers = self._get_headers()
        async with aiohttp.ClientSession(headers=headers) as client:
            async with client.get(url, json=json) as query:
                return await query.read()

    def _get_headers(self):
        return {}
