import os
from http_client import HttpClient
import asyncio

SEMAPHORE_DOWNLOAD = asyncio.Semaphore(3)

class ProviderClient(HttpClient):

    base_url = "https://api.1fichier.com"
    get_token_endpoint = "v1/download/get_token.cgi"

    def __init__(self, token):
        self.token = token
        super().__init__(self.base_url)

    def _get_headers(self):
        res = super()._get_headers()
        res.update(
            {
                "Authorization": f"Bearer {self.token}"
            }
        )
        return res

    def url_to_json(self, url):
        url = url.replace("\\", "")
        url = url.replace("\n", "")
        url, filename = url.rsplit("/", 1)
        url, _ = url.rsplit("&", 1)
        return {
            "url": url,
            "filename": filename
        }

    def get_download_url(self):
        return f"{self.base_url}/{self.get_token_endpoint}"

    async def download(self, url, out_path):
        if not os.path.exists(out_path):
            os.makedirs(out_path, exist_ok=True)
        json = self.url_to_json(url)
        response = await self.post(self.get_download_url(), json)
        if response.get("status") != "OK":
            raise Exception(response)
        url = response.get("url")
        filename = json.get("filename")
        file_path = os.path.join(out_path, filename)
        async with SEMAPHORE_DOWNLOAD:
            file_path = os.path.join(out_path, filename)
            print(f"Downloading {filename}")
            raw_data = await self.get(url)
            with open(file_path, "wb") as f:
                f.write(raw_data)
            print(f"{file_path} done")
