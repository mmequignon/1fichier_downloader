#!/usr/bin/env python3

import os
import yaml

yaml_vals = """
folder:
  subfolder1:
    urls:
      - url1
      - url2
  subfolder2:
    subfolder3:
      urls: 
        - url3
    urls:
      - url4
"""

class YamlParser:

    def __init__(self, filename):
        self.filename = filename
        self.setup_content()
        self.parse()

    def setup_content(self):
        with open(self.filename, "rb") as f:
            self.content = yaml.safe_load(f)

    def recursive_parse(self, content, path=None):
        res = {}
        urls = content.pop("urls", None)
        if urls:
            res[path] = urls
        for folder, subcontent in content.items():
            node_path = path and os.path.join(path, folder) or folder
            node_res = self.recursive_parse(subcontent, node_path)
            res.update(node_res)
        return res

    def parse(self):
        # We get yaml with this format
        # folder:
        #   subfolder1:
        #     urls:
        #       - url1
        #       - url2
        #   subfolder2:
        #     subfolder3:
        #       urls:
        #         - url3
        #     urls:
        #       - url4
        # returns {
        #   "folder/subfolder1": ["url1", "url2"]
        #   "folder/subfolder2": ["url4"]
        #   "folder/subfolder2/subfolder3": ["url3"]
        # }
        self.parsed_content = self.recursive_parse(self.content)
