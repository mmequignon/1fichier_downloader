1fichier downloader
===================

How to use
----------

```bash
API_KEY="<YOUR API KEY>" URLS_FILENAME="<PATH TO FILENAME>" OUT_FOLDER="<PATH TO OUTPUT>" ./main.py
```

Environment variables
_____________________

This script uses environment variables to get the api key, the output folder and the urls filename
Those variables are the following:
 - API_KEY
 - URLS_FILENAME
 - OUT_FOLDER

Url file
________

The url file uses the yaml syntax and should look like this:

```yaml
folder1:
  subfolder1:
    urls:
      - <URLS>
  subfolder2:
    sub-subfolder3:
      urls:
        - <URLS>
```

With the following urls file, you will get the following tree in your output folder

```
├── folder1
│   ├── subfolder1
│   │   └── <DOWNLOADED FILES>
│   ├── subfolder2
│   │   ├── sub-subfolder3
│   │   │   └── <DOWNLOADED FILES>
```

This is useful for pre-ordering downloaded files

API_KEY
_______

Because the 1fichier API is only available for premium users, this script will only work if you have a premium account.
